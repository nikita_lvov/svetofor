package com.example.trafficlight;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity {
    private LinearLayout b1, b2, b3;
    private Button button;
    private boolean start_stop = false;
    private int counter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        b1 = findViewById(R.id.bulb1);
        b2 = findViewById(R.id.bulb2);
        b3 = findViewById(R.id.bulb3);
        button = findViewById(R.id.button);
    }

    @SuppressLint("SetTextI18n")
    public void onClickStart(View view) {
        if (!start_stop) {
            button.setText("Stop");
            button.setBackgroundColor(getResources().getColor(R.color.red));
            start_stop = true;

            new Thread(new Runnable() { // создание нового потока
                @Override
                public void run() {
                    while (start_stop) { // запуск цикла
                        counter++;
                        switch (counter) {
                            case 1:
                                b1.setBackgroundColor(getResources().getColor(R.color.red));
                                b2.setBackgroundColor(getResources().getColor(R.color.grey));
                                b3.setBackgroundColor(getResources().getColor(R.color.grey));
                                break;
                            case 2:
                                b1.setBackgroundColor(getResources().getColor(R.color.grey));
                                b2.setBackgroundColor(getResources().getColor(R.color.yellow));
                                b3.setBackgroundColor(getResources().getColor(R.color.grey));
                                break;
                            case 3:
                                b1.setBackgroundColor(getResources().getColor(R.color.grey));
                                b2.setBackgroundColor(getResources().getColor(R.color.grey));
                                b3.setBackgroundColor(getResources().getColor(R.color.green));
                                counter = 0;
                                break;
                        }


                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }).start();
        } else {
            start_stop = false;
            button.setText("Start");
            button.setBackgroundColor(getResources().getColor(R.color.white));
            b1.setBackgroundColor(getResources().getColor(R.color.grey));
            b2.setBackgroundColor(getResources().getColor(R.color.grey));
            b3.setBackgroundColor(getResources().getColor(R.color.grey));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        start_stop = false; // при выходе из приложения цикл останавливается
    }
}
